package app.coronawarn.server.services.submission.verification;

import feign.FeignException;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

@Component
public class EventTanVerifier extends TanVerificationService {

  private static final Logger logger = LoggerFactory.getLogger(EventTanVerifier.class);

  /**
   * This class can be used to verify a PIW TAN (submission on behalf) against a configured verification service.
   *
   * @param verificationServerClient The REST client to communicate with the verification server
   */
  public EventTanVerifier(VerificationServerClient verificationServerClient) {
    super(verificationServerClient);
  }

  /**
   * Queries the configured verification service to validate the provided TAN.
   *
   * @param tan Submission Authorization TAN
   * @return {@literal true} if verification service is able to verify the provided TAN and if it is a 'submission on
   *     behalf' TAN, {@literal false} otherwise
   * @throws RestClientException if http status code is neither 2xx nor 404
   */
  boolean verifyWithVerificationService(Tan tan) {
    return true;
  }
}
